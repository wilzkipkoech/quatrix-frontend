import React, { Component } from 'react'
import AuthenticationService from '../service/AuthenticationService';
import {  Row,Col } from 'react-bootstrap';

class LoginComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.loginClicked = this.loginClicked.bind(this)
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]
                    : event.target.value
            }
        )
    }

    loginClicked() {
    

        AuthenticationService
            .executeJwtAuthenticationService(this.state.username, this.state.password)
            .then((response) => {

                console.log(response.data.token)
                
                AuthenticationService.registerSuccessfulLoginForJwt(this.state.username, response.data.token)
                this.props.history.push(`/task/assigned`)
            }).catch(() => {
                this.setState({ showSuccessMessage: false })
                this.setState({ hasLoginFailed: true })
            })

    }

    render() {
        return (
            <div>
                    <Row className="show-grid" float="center">
                    <Col xs={12} xsOffset={6}>
                    <h1>Login</h1>
                <div className="container">
                    
                    {/*<ShowInvalidCredentials hasLoginFailed={this.state.hasLoginFailed}/>*/}
                    {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid User  Credentials</div>}
                    {this.state.showSuccessMessage && <div>Login Sucessful</div>}
                    {/*<ShowLoginSuccessMessage showSuccessMessage={this.state.showSuccessMessage}/>*/}
                    <label className="col-sm-0 control-label" htmlFor="textinput"> Name : &nbsp; </label>
                  <input type="text"  name="username" value={this.state.username} onChange={this.handleChange}  placeholder="Name" className="form-control"></input>
                  
                  <label className="col-sm-0 control-label" htmlFor="textinput"> Password: &nbsp; </label>
                  <input type="password" name="password" value={this.state.password} onChange={this.handleChange}  placeholder="Name" className="form-control"></input>
                  <br></br>
                    <button className="btn btn-success" onClick={this.loginClicked}>Login</button>
                </div>
                    </Col>
                    </Row>



              
            </div>
        )
    }
}

export default LoginComponent
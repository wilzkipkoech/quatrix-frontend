import React, { Component } from 'react'
import TaskDataService from '../service/TaskDataService.js';



class TaskListComponents extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tasks: [],
            message: null
        }
        this.refreshTask = this.refreshTask.bind(this)
    }

    componentDidMount() {
        this.refreshTask();
        
    }

    refreshTask() {
        TaskDataService.retrieveAllTask()
            .then(
                response => {
                    this.setState({ tasks: response.data })
                  
                }
            )
    }


    render() {
        console.log('render')
        return (
            <div className="container">
                <h3>All Tasks</h3>
                <div className="container">
                <table className="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Customer First Name</th>
                                <th>Customer Last Name</th>
                                <th>Personal First Name</th>
                                <th>Personal Other Name</th>
                                <th>Customer Phone</th>
                                <th>Agent Id</th>
                                <th>Deffered</th>
                                <th>Age</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.tasks.map(
                                    task =>
                                        <tr key={task.id}>
                                            <td>{task.id}</td>
                                            <td>{task.customer_first_name}</td>
                                            <td>{task.customer_last_name}</td>
                                            <td>{task.personal_first_name}</td>
                                            <td>{task.personal_other_name}</td>
                                            <td>{task.cutomer_phone}</td>
                                            <td>{task.agent_id}</td>
                                            <td>{task.agent_deffered}</td>
                                            <td>{task.agent_age}</td>
                                            <td>{task.comments}</td>
                                           
                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default TaskListComponents
